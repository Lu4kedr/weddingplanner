﻿using System;
using System.Collections.Generic;
using System.Text;

using Application.Interfaces;

using Domain.Entities;

namespace Infrastructure.Persistence.Repositories
{
    public class WedinngGiftRepository: Repository<WeddingGift>, IWedinngGiftRepository
    {
        public WedinngGiftRepository() : base()
        {

        }
    }
}
