﻿
using Application.Interfaces;

using Domain.Entities;

namespace Infrastructure.Persistence.Repositories
{
    public class WedinngItineraryRepository : Repository<WeddingGift>, IWedinngItineraryRepository
    {
        public WedinngItineraryRepository():base()
        {

        }
    }
}
