﻿
using Application.Interfaces;

using Domain.Entities;

namespace Infrastructure.Persistence.Repositories
{
    public class WedinngRepository : Repository<Wedding>, IWedinngRepository
    {
        public WedinngRepository() : base()
        {

        }
    }
}
