﻿
using Application.Interfaces;

using Domain.Entities;

namespace Infrastructure.Persistence.Repositories
{
    public class AppUserRepository : Repository<AppUser>, IAppUserRepository
    {
        public AppUserRepository() : base()
        {

        }
    }
}
