﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

using Application.Interfaces;

using Domain.Entities;

using Infrastructure.Persistence.DataMappers;

namespace Infrastructure.Persistence.Repositories
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        private DataMapper<TEntity> dataMapper;

        public Repository(DataMapper<TEntity> dataMapper)
        {
            this.dataMapper = dataMapper;
        }
        public void Add(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public TEntity Get(long id)
        {
            throw new NotImplementedException();
        }

        public IList<TEntity> GetAll()
        {
            return dataMapper.GetAll();
        }

        public void Remove(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Update(TEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}
