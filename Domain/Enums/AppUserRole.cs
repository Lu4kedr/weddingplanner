﻿namespace Domain.Enums
{
    public enum AppUserRole
    {
        Administrator,
        Organizer,
        User
    }
}
