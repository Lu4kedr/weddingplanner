﻿namespace Domain.Enums
{
    public enum ItineraryState
    {
        NotStarted,
        InProgress,
        Complete
    }
}
