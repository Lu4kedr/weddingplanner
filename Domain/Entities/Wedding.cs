﻿using System.Collections.Generic;

namespace Domain.Entities
{
    public class Wedding : Entity
    {
        public Address CeremonyAddress { get; set; }
        public Address CelebrationAddress { get; set; }
        public int GuestCappacity { get; set; }
        public IList<WeddingGift> WeddingGifts { get; set; }
    }
}
