﻿using Domain.Enums;

namespace Domain.Entities
{
    public class WeddingItineraryItem : Entity
    {
        public string Name { get; set; }
        public ItineraryState State { get; set; }
        public string Description { get; set; }
    }
}
