﻿using System.Collections.Generic;

namespace Domain.Interfaces
{
    public interface IWedinngGiftManagerService
    {
        public void CreateGiftList(IEnumerable<(string Name, string Link)> gifts, int wedinngID);
        public bool RegisterGift(string giftId, string userId);
    }
}
